% Author: Ing. Milan Ondrašovič, milan.ondrasovic@gmail.com

\documentclass{beamer}
\usetheme{Boadilla}

% *****************************************************************************
% PACKAGES

% Use UTF-8 encoding.
\usepackage[utf8]{inputenc}

% Presentatio-related packages.
\usepackage{beamerthemesplit}
\setbeamertemplate{navigation symbols}{} %no nav symbols
\usetheme{lankton-keynote}

% Mathematical formulas.
\usepackage{amsmath}
\usepackage{amssymb}

% Ceiling, floor, etc.
\usepackage{mathtools}

% Quotations.
\usepackage{epigraph}

\setlength\epigraphwidth{\textwidth}
\setlength\epigraphrule{0.02pt}

\usepackage{tkz-graph}

\usepackage{pgfplots}
\pgfplotsset{width=7.5cm,compat=1.12}
\usepgfplotslibrary{fillbetween}

\GraphInit[vstyle = Shade]
\tikzset{
  LabelStyle/.style = { rectangle, rounded corners, draw,
                        minimum width = 1em, fill = yellow!30,
                        text = red, font = \tiny\bfseries },
  VertexStyle/.append style = { rectangle, inner sep=3pt,
                                font = \tiny\bfseries},
  EdgeStyle/.append style = {->}
}
  
\setbeamertemplate{footline}
{%
   \leavevmode%
   \hbox{\begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm plus1fill,rightskip=.3cm]{author in head/foot}%
     \usebeamerfont{author in head/foot}\insertshortauthor
   \end{beamercolorbox}%
   \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm,rightskip=.3cm plus1fil]{title in head/foot}%
     \usebeamerfont{title in head/foot}
     \hspace*{\fill}
     \textbf{MiST} conference \quad | \quad Deep learning \qquad\qquad \insertframenumber \quad of \quad \inserttotalframenumber
   \end{beamercolorbox}}%
   \vskip0pt%
}

\newcommand{\bookpage}[1]{[\textit{page \textbf{#1}}]}

\title{\textbf{Deep learning} - partial \textit{book review}}
\subtitle{\textbf{Ian Goodfellow}, \textbf{Yoshua Bengio}, \textbf{Aaron Courville}}
\author{Ing. \textbf{Milan Ondrašovič}}
\institute{\small{\textbf{Department of mathematical methods and operations research}}}
\date{}

% *****************************************************************************
% MAIN DOCUMENT BODY
\begin{document}

% -----------------------------------------------------------------------------
\begin{frame}
\titlepage
\end{frame}

\section{Chapter 1: Introduction}

\subsection{Chapters overview - part I.}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Chapter prerequisites}

\begin{center}
  \begin{tikzpicture}
    \SetGraphUnit{4}
    
    \Vertex[L=(1) Introduction]{A}
    \EA[L=(2) Linear Algebra](A){B}
    \SO[L=(3) Probability and Information Theory](B){C}
    \EA[L=(4) Numerical Computation](B){D}
    \SO[L=(5) Machine Learning Basics](D){E}
    
    \Edge(A)(B)
    \Edge(A)(C)
    \Edge(B)(C)
    \Edge(B)(D)
    \Edge(D)(E)
    \Edge(C)(E)
  \end{tikzpicture}
\end{center}
\end{frame}

\subsection{Key insights}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Key insights}

\epigraph{The \textbf{true challenge} to \textbf{artificial intelligence} proved to be solving the \textbf{tasks that are easy for people to perform but hard for people to describe}, like recognizing spoken words or faces in images.}{\bookpage{1}}

\epigraph{The difficulties faced by systems relying on hard-coded knowledge suggest that \textbf{AI systems need the ability to acquire their own knowledge}, by extracting patterns from raw data. This capability is known as \textbf{machine learning}.}{\bookpage{2}}
\end{frame}

\subsection{Data representation}
% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Data representation}

\begin{itemize}
\item \textbf{Separate} the \textbf{two categories} by a \textbf{line between them} \bookpage{4}.
\begin{itemize}
  \item Plot on the \textbf{left} $\to$ \textbf{Cartesian} coordinates $\to$ \textbf{impossible}.
  \item Plot on the \textbf{right} $\to$ \textbf{Polar} coordinates $\to$ \textbf{trivial}.
\end{itemize}
\end{itemize}

\begin{tikzpicture}[scale=0.6]

  \pgfplotsset{
      scale only axis,
  }

  \begin{axis}[
    xlabel=$x$,
    ylabel=$y$,
  ]
  
  \addplot[only marks, mark=x, color=red]
  coordinates{
  (0.32, 0.06) (8.59, 4.79) (-9.50, 3.09) (0.35, 0.41) (8.32, -2.04)
  (-1.57, -2.97) (7.21, -5.97) (-4.29, -1.79) (6.52, -5.54) (4.46, 7.83)
  (-2.71, -0.46) (-0.23, -6.93) (-7.75, -0.08) (6.26, -4.90) (5.13, 0.50)
  (-7.85, 5.77) (1.08, -3.20) (9.46, -0.36) (-5.63, -5.73) (8.21, 4.69)
  (4.58, 3.22) (2.47, 1.58) (-0.75, 2.42) (-1.79, -4.18) (-0.74, -1.57)
  (-2.81, 0.81) (4.13, 8.93) (2.91, 3.52) (3.78, -2.57) (6.74, 5.92)
  (7.56, -5.72) (2.23, 4.92) (-0.43, 1.00) (2.66, -1.60) (-4.74, -3.26)
  (5.29, 6.12) (-0.63, 7.34) (-1.89, 0.79) (4.03, 6.11) (-0.19, -4.03)
  (6.16, -5.20) (5.97, 3.74) (-4.62, 0.09) (0.15, -0.61) (4.62, -4.56)
  (-7.90, -5.78) (0.57, 1.57) (6.61, 1.80) (-2.77, -1.25) (3.04, -3.90)
  };
  
  \addplot[only marks, mark=o, color=green]
  coordinates{
  (-20.03, -7.37) (-20.80, -4.85) (-14.15, 14.22) (18.32, -1.39) (-9.85, -9.61)
  (0.84, 14.25) (11.12, 5.64) (-10.95, -8.89) (-10.48, -6.34) (9.98, 14.55)
  (-16.42, -7.25) (-16.79, 13.01) (6.40, 14.95) (-14.39, -0.97) (12.97, -8.23)
  (-18.41, 8.60) (-6.84, 14.83) (4.06, 16.22) (12.17, 16.46) (-14.11, 6.07)
  (2.75, -12.12) (4.32, 16.83) (-9.59, 10.52) (-12.92, -5.20) (-17.45, -4.49)
  (-10.84, 9.02) (-15.55, -14.40) (8.49, -9.54) (2.87, -13.80) (-1.56, -18.31)
  (-8.54, -12.27) (12.97, 14.22) (-2.59, 18.73) (-9.76, 12.72) (-16.56, 5.71)
  (-20.51, -3.82) (1.50, -15.49) (8.80, -15.16) (9.76, -16.08) (-5.38, 13.99)
  (10.10, 16.23) (-0.94, -18.49) (9.82, -18.33) (17.04, -0.81) (-6.75, 11.87)
  (-9.42, -15.93) (-1.23, 13.03) (-6.36, -13.12) (12.80, -16.36) (6.87, 15.91)
    };
  \end{axis}

\end{tikzpicture}
\qquad
\begin{tikzpicture}[scale=0.6]

  \pgfplotsset{
      scale only axis,
  }

  \begin{axis}[
    xlabel=$r$,
    ylabel=$\theta$,
  ]
  
  \addplot[only marks, mark=x, color=red]
  coordinates{
  (5.07, 5.45) (5.53, 1.66) (5.69, 1.62) (8.44, 2.74) (6.98, 1.48)
  (6.83, 0.72) (4.84, 5.59) (7.77, 2.60) (4.46, 2.70) (3.90, 0.86)
  (6.79, 2.42) (9.04, 4.41) (0.20, 2.61) (3.03, 4.54) (3.04, 3.57)
  (5.57, 5.24) (6.99, 3.22) (8.42, 2.74) (6.44, 2.03) (8.73, 3.51)
  (1.44, 3.98) (6.00, 1.74) (6.25, 4.52) (7.45, 4.05) (7.37, 3.05)
  (1.86, 4.26) (9.81, 4.62) (3.02, 4.19) (4.83, 4.47) (9.51, 4.27)
  (5.88, 2.76) (7.09, 3.20) (3.17, 6.14) (8.64, 4.21) (7.91, 0.22)
  (6.58, 1.13) (3.52, 2.49) (3.39, 6.28) (5.94, 2.87) (5.15, 2.99)
  (6.16, 4.03) (3.69, 1.75) (7.14, 2.69) (5.65, 0.90) (0.64, 4.48)
  (9.42, 4.73) (8.75, 2.78) (3.45, 5.88) (1.00, 5.41) (3.98, 0.89)
  };
  
  \addplot[only marks, mark=o, color=green]
  coordinates{
  (21.56, 0.98) (18.20, 0.39) (19.24, 3.07) (18.09, 2.00) (19.39, 0.67)
  (21.55, 4.84) (15.65, 5.91) (18.96, 2.63) (15.08, 6.26) (15.85, 3.83)
  (14.37, 2.51) (20.01, 4.37) (15.13, 0.97) (21.76, 2.88) (17.22, 2.51)
  (20.53, 6.23) (18.25, 1.93) (18.00, 0.24) (15.33, 3.65) (14.49, 4.05)
  (14.51, 3.83) (20.79, 0.07) (13.41, 4.26) (16.01, 1.12) (19.36, 4.67)
  (18.98, 3.79) (19.14, 1.72) (14.21, 4.21) (13.69, 5.01) (14.48, 4.75)
  (15.54, 0.61) (13.76, 2.14) (17.12, 0.06) (13.98, 6.03) (14.33, 5.28)
  (20.29, 1.63) (13.44, 1.10) (14.35, 0.78) (14.27, 0.96) (15.24, 5.53)
  (15.43, 1.22) (19.74, 4.12) (21.86, 4.43) (21.24, 5.62) (14.27, 2.17)
  (20.13, 4.57) (19.91, 5.00) (13.25, 6.15) (18.51, 2.79) (18.15, 0.03)
    };
  \end{axis}

\end{tikzpicture}

\end{frame}

\subsection{History}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{History}

\epigraph{\textbf{Broadly speaking}, there have been \textbf{three waves} of \textbf{development} of \textbf{deep learning}: deep learning known as \textbf{cybernetics} in the \textbf{1940s–1960s}, deep learning known as \textbf{connectionism} in the \textbf{1980s–1990s}, and the \textbf{current resurgence} under the name \textbf{deep learning beginning in 2006}.} {\bookpage{13}}
\end{frame}

\section{Chapter 2: Linear Algebra}

\subsection{Basic definitions}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Basics of linear algebra}

\epigraph{A good \textbf{understanding} of \textbf{linear algebra} is \textbf{essential} for \textbf{understanding} and \textbf{working} with \textbf{many machine learning algorithms}, especially \textbf{deep learning algorithms}.}{\bookpage{31}}

\begin{itemize}
\item \textbf{Definitions}: \textbf{scalar}, \textbf{vector}, \textbf{matrix} and \textbf{tensor} \bookpage{31}.
\item \textbf{Vector} and \textbf{matrix multiplication} \bookpage{34}.
\item \textbf{Identity} and \textbf{inverse matrix} \bookpage{36}.
\item Linear \textbf{dependence} and \textbf{span} \bookpage{37}.
\item \textbf{Determinant} \bookpage{47}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Other types of matrices}
\begin{itemize}
\item \textbf{Orthogonal} matrix: a \textbf{square matrix} whose \textbf{rows are mutually orthonormal} and whose \textbf{columns are mutually orthonormal} \bookpage{41}.
\begin{equation}
\textbf{A}^T\textbf{A} =
\textbf{A}\textbf{A}^T =
\textbf{I}
\to
\textbf{A}^{-1} = \textbf{A}^T.
\end{equation}
\end{itemize}
\end{frame}

\subsection{Operations with matrices}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Eigendecomposition}

\begin{itemize}
\item An \textbf{eigenvector} of a \textbf{square matrix} $\textbf{A}$ is a \textbf{non-zero vector} $\textbf{v}$ such that \textbf{multiplication} by  $\textbf{A}$ alters only the \textbf{scale} of $\textbf{v}$:
\begin{equation}
\textbf{A}\textbf{v} = \lambda \textbf{v}
\end{equation}
The scalar $\lambda$ is known as the \textbf{eigenvalue} corresponding to this \textbf{eigenvector} \bookpage{42}.

\item The \textbf{eigendecomposition} of $\textbf{A}$ is then given by \bookpage{43}
\begin{equation}
\textbf{A} = \textbf{V} \text{diag} \left( \boldsymbol{\lambda} \right) \textbf{V}^{-1}.
\end{equation}
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Singular Value Decomposition (SVD)}

\begin{itemize}
\item The \textbf{eigendecomposition} involves analyzing a matrix $\textbf{A}$ to discover a \textbf{matrix} $\textbf{V}$ of \textbf{eigenvectors} and a \textbf{vector} of \textbf{eigenvalues} $\boldsymbol{\lambda}$ such that we can \textbf{rewrite} $\textbf{A}$ as \bookpage{45}

\begin{equation}
\textbf{A} = \textbf{V} \text{diag} \left( \boldsymbol{\lambda} \right) \textbf{V}^{-1}.
\end{equation}

\item The \textbf{singular value decomposition} is \textbf{similar}, except this time we will write $\textbf{A}$ as a \textbf{product} of \textbf{three matrices} \bookpage{45}:

\begin{equation}
\textbf{A} = \textbf{U}\textbf{D}\textbf{V}^T.
\end{equation}
\end{itemize}

\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{The Moore-Penrose Pseudoinverse}

\begin{itemize}
\item \textbf{Matrix inversion} is \textbf{not defined} for matrices that are \textbf{not square} \bookpage{45}.
\item \textbf{Practical algorithms} for computing the \textbf{pseudoinverse} are based on the formula

\begin{equation}
\textbf{A}^+ = \textbf{V}\textbf{D}^+\textbf{U}^T,
\end{equation}

where $\textbf{U}$, $\textbf{D}$ and $\textbf{V}$ are the \textbf{SVD} of $\textbf{A}$, and the \textbf{pseudoinverse} $\textbf{D}^+$ of a \textbf{diagonal} matrix $\textbf{D}$ is obtained by taking the \textbf{reciprocal of its non-zero elements} then taking the \textbf{transpose} \bookpage{46}.
\end{itemize}
\end{frame}

\subsection{Putting it all together}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Principal Component Analysis (PCA)}

\begin{itemize}
\item We have a \textbf{collection} of $m$ points $\left\{ \textbf{x}^{(1)}, \textbf{x}^{(2)}, ..., \textbf{x}^{(m)}\right\}$ in $\mathbb{R}^n$ . We would like to apply \textbf{lossy compression} to these points. Lossy compression means \textbf{storing the points in a way that requires less memory but may lose some precision}. We would like to \textbf{lose as little precision as possible} \bookpage{48}.
\item One way we can \textbf{encode} these points is to \textbf{represent} a \textbf{lower-dimensional version} of them. For each point $\textbf{x}^{(i)} \in \mathbb{R}^n$ we will find a \textbf{corresponding code vector} $\textbf{c}^{(i)} \in \mathbb{R}^l$ . If $l$ is \textbf{smaller} than $n$, it will take \textbf{less memory to store the code points} than the original data \bookpage{48}.
\end{itemize}

\end{frame}

\section{Chapter 3: Probability and Information Theory}

\subsection{Basic probability definitions}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Basic definitions}

\begin{itemize}
\item \textbf{Random variable} \bookpage{56}.
\item \textbf{Probability distribution} \bookpage{56}.
\item \textbf{Probability mass function} (PMF) \bookpage{56}.
\item \textbf{Probability density function} (PDF) \bookpage{58}.
\item \textbf{Marginal probability}: a \textbf{probability distribution} over a \textbf{subset} \bookpage{58}.
\item \textbf{Conditional probability} \bookpage{59}
\begin{equation}
\text{P} \left( Y = y \text{ | } X = x \right) = 
\frac{
  \text{P} \left( Y = y, X = x \right)
}{
  \text{P} \left( X = x \right)
}.
\end{equation}
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Basic definitions}

\begin{itemize}
\item \textbf{Chain rule} or \textbf{product rule} of \textbf{conditional probabilities} \bookpage{59} (example):
\begin{equation}
\begin{aligned}
\text{P} \left( a, b, c \right) &=
\text{P} \left( a \text{ | } b, c \right)
\text{P} \left( b, c \right)
\\
\text{P} \left( b, c \right) &=
\text{P} \left( b \text{ | } c \right)
\text{P} \left( c \right)
\\
\text{P} \left( a, b, c \right) &=
\text{P} \left( a \text{ | } b, c \right)
\text{P} \left( b, c \right)
\text{P} \left( c \right).
\end{aligned}
\end{equation}
\item \textbf{Independence} and \textbf{conditional independence} \bookpage{60}.
\item \textbf{Expectation}, \textbf{variance} and \textbf{covariance} \bookpage{60}.
\item \textbf{Central limit theorem} \bookpage{64}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Probability distributions}

\begin{itemize}
\item \textbf{Discrete uniform} distribution \bookpage{57}.
\item \textbf{Bernoulli} distribution \bookpage{62}.
\item \textbf{Multinoulli} or \textbf{categorical} distribution: \textbf{distribution} over a \textbf{single discrete variable} with $k$ \textbf{different states}, where $k$ is \textbf{finite} \bookpage{62}.
\item \textbf{Gaussian} distribution \bookpage{63}.
\item \textbf{Multivariate normal} distribution \bookpage{64}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Probability distributions}

\begin{itemize}
\item \textbf{Exponential} distribution \bookpage{65}.
\item \textbf{Laplace} distribution: a closely \textbf{related} to \textbf{exponential probability} distribution that allows us to \textbf{place} a \textbf{sharp peak} of \textbf{probability mass} at an \textbf{arbitrary point} $\mu$ \bookpage{65}.
\item \textbf{Dirac} distribution: used when we wish to \textbf{specify} that \textbf{all of the mass} in a \textbf{probability distribution} \textbf{clusters} around a \textbf{single point} \bookpage{65}.
\item \textbf{Empirical} distribution \bookpage{66}.
\item \textbf{Mixtures} of \textbf{distributions}: it is made up of \textbf{several component distributions}, useful to \textbf{define probability distributions} by \textbf{combining other simpler probability distributions} \bookpage {66}.
\end{itemize}
\end{frame}

\subsection{Basic information theory definitions}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Basic definitions}

\begin{itemize}
\item The \textbf{Shannon entropy}: a \textbf{quantification} of the \textbf{amount of uncertainty} in an \textbf{entire probability distribution} given by \bookpage{74}
\begin{equation}
\text{H} \left( X \right) =
\mathbb{E}_{x \sim P}
\left[
  \text{I} \left( x \right)
\right] =
-\mathbb{E}_{x \sim P}
\left[
  \log \left(
    \text{P} \left( x \right)
  \right)
\right].
\end{equation}
\item \textbf{Kullback}-\textbf{Leibler} (\textbf{KL}) \textbf{divergence}: \textbf{measure} of \textbf{how different two probability distributions} are given by \bookpage{74}
\begin{equation}
\begin{aligned}
\text{D}_{KL} \left( P \Vert Q \right) &=
\mathbb{E}_{x \sim P}
\left[
  \log \left(
    \frac{
      \text{P} \left( x \right)
    }{
      \text{Q} \left( x \right)
    }
  \right)
\right]\\ &=
-\mathbb{E}_{x \sim P}
\left[
  \log \left(
    \text{P} \left( x \right)
  \right)
  -
  \log \left(
    \text{Q} \left( x \right)
  \right)
\right].
\end{aligned}
\end{equation}
\end{itemize}
\end{frame}

\subsection{Probabilistic models}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Structured probabilistic models}

\begin{itemize}
\item Instead of using a \textbf{single function} to \textbf{represent a probability distribution}, we can \textbf{split a probability distribution} into \textbf{many factors} that \textbf{we multiply together} \bookpage{77}.
\item These \textbf{factorizations} can \textbf{greatly reduce the number of parameters} needed to \textbf{describe} the \textbf{distribution} \bookpage{77}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Structured probabilistic models}

The \textbf{graph} below corresponds to \textbf{probability distributions} that can be \textbf{factored} as
\begin{equation}
\text{P} \left( a, b, c, d, e \right) =
\text{P} \left( a \right)
\text{P} \left( b \text{ | } a \right)
\text{P} \left( c \text{ | } a, b \right)
\text{P} \left( d \text{ | } b \right)
\text{P} \left( e \text{ | } c \right).
\end{equation}

\begin{center}
  \begin{tikzpicture}
    \SetGraphUnit{3}
    
    \Vertex{A}
    \EA(A){B}
    \SO(A){C}
    \EA(C){D}
    \WE(C){E}
    
    \Edge(A)(B)
    \Edge(A)(C)
    \Edge(B)(C)
    \Edge(B)(D)
    \Edge(C)(E)
  \end{tikzpicture}
\end{center}
\end{frame}

\section{Chapter 4: Numerical Computation}

\subsection{Basic terminology}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Basic terminology}

\begin{itemize}
\item \textbf{Underflow}: occurs when numbers \textbf{near zero} are \textbf{rounded to zero} \bookpage{80}.
\item \textbf{Overflow}: occurs when numbers with \textbf{large magnitude} are \textbf{approximated} as $\infty$ or $-\infty$ \bookpage{81}.
\item \textbf{Poor conditioning}: consider the function $f \left( \textbf{x} \right) = \textbf{A}^{-1}\textbf{x}$, such that $\textbf{A} \in \mathbb{R}^{n \times n}$ has an \textbf{eigenvalue decomposition}, then its \textbf{condition number} is \bookpage{82}
\begin{equation}
\begin{aligned}
  \underset{i, j} {\text{max}}
    \left\vert
    \frac{
      \lambda_i
    }{
      \lambda_j
    }
  \right\vert.
\end{aligned}
\end{equation}
\end{itemize}
\end{frame}

\subsection{Optimization}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Gradient-based optimization}

\begin{itemize}
\item The \textbf{function} we want to \textbf{minimize} or \textbf{maximize} is called the \textbf{objective function} or \textbf{criterion}. When we are \textbf{minimizing} it, we may also call it the \textbf{cost function}, \textbf{loss function}, or \textbf{error function} \bookpage{82}.
\item The \textbf{derivative} is useful for \textbf{minimizing} a function because it tells us \textbf{how to change} $x$ in order to make a \textbf{small improvement} in $y$ \bookpage{83}.
\item We can thus \textbf{reduce} $\text{f} \left( x \right)$ by \textbf{moving} $x$ in \textbf{small steps} with \textbf{opposite sign} of the \textbf{derivative}. This technique is called \textbf{gradient descent} \bookpage{83}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Derivatives in multiple dimensions}

\begin{itemize}
\item \textbf{Jacobian matrix}: if we have a function $\text{f}: \mathbb{R}^m \to \mathbb{R}^n$, then the \textbf{Jacobian matrix} $\textbf{J} \in \mathbb{R}^n \times \mathbb{R}^m$ of $\text{f}$ is defined such that $\textbf{J}_{i, j} = \frac{\partial}{\partial x_j} \text{f} {\left( \textbf{x} \right)}_i$ \bookpage{86}.
\item \textbf{Hessian matrix}: when our function has \textbf{multiple input dimensions}, there are \textbf{many second derivatives}, so these derivatives can be \textbf{collected together} into a \textbf{matrix} called the \textbf{Hessian matrix} defined such that ${\textbf{H} \left( \text{f} \right) \left( \textbf{x} \right)}_{i, j} = \frac{{\partial}^2}{\partial x_i \partial x_j} \text{f} {\left( \textbf{x} \right)}_i$ \bookpage{87}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Constrained optimization}

\begin{itemize}
\item In the context of deep learning, we sometimes \textbf{gain some guarantees} by \textbf{restricting ourselves} to functions that are either \textbf{Lipschitz continuous} or have \textbf{Lipschitz continuous derivatives} \bookpage{92}.
\item A \textbf{Lipschitz continuous} function is a function $f$ whose \textbf{rate of change} is \textbf{bounded} by a \textbf{Lipschitz constant} $\omega$ \bookpage{92}:
\begin{equation}
\forall \textbf{x}, \forall \textbf{y},
\left\vert
  \text{f} \left( \textbf{x} \right)
  -
  \text{f} \left( \textbf{y} \right)  
\right\vert
\leq
\omega
{\left\Vert
  \textbf{x} - \textbf{y}
\right\Vert}_2.
\end{equation}
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Constrained optimization}

\begin{itemize}
\item The \textbf{Karush}–\textbf{Kuhn}–\textbf{Tucker} (\textbf{KKT}) approach provides a \textbf{very general solution} to \textbf{constrained optimization} \bookpage{94}.
\item The equations involving $\text{g}^{(i)}$ are called the \textbf{equality constraints} and the inequalities involving $\text{h}^{(j)}$ are called \textbf{inequality constraints} \bookpage{94}.
\item The \textbf{generalized Lagrangian} is then defined as
\begin{equation}
\text{L} \left(
  \textbf{x},
  \boldsymbol{\lambda},
  \boldsymbol{\alpha}
\right) =
\text{f} \left( \textbf{x} \right) +
\sum_i {
  \lambda_i
  \text{g}^{(i)} \left( \textbf{x} \right)
} +
\sum_j {
  \alpha_j
  \text{h}^{(j)} \left( \textbf{x} \right),
}
\end{equation}
allowing us to solve a \textbf{constrained minimization problem} using \textbf{unconstrained optimization} of the \textbf{generalized Lagrangian} \bookpage{94}.
\end{itemize}
\end{frame}

\subsection{Useful formulas}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Useful formulas}

\begin{itemize}
\item \textbf{Softmax} function \bookpage{81}.
\begin{equation}
\text{softmax} \left( \textbf{x} \right)_i =
\frac{
  e^{x_i}
}{
  \sum_{j = 1}^n {
    e^{x_j}
  }
}
\end{equation}
\end{itemize}
\end{frame}

\section{Chapter 5: Machine Learning Basics}

\subsection{Basic introduction}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Machine learning algorithm}

\epigraph{A \textbf{computer program} is said to \textbf{learn} from experience \textbf{E} with respect to some \textbf{class of tasks} \textbf{T} and \textbf{performance measure} \textbf{P}, if its \textbf{performance} at \textbf{tasks} in \textbf{T}, as \textbf{measured} by \textbf{P}, \textbf{improves} with \textbf{experience} \textbf{E}.}{\bookpage{99}}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{The task, T}

\begin{itemize}
\item The \textbf{task T}: if we want a \textbf{robot to be able to walk}, then \textbf{walking is the task} \bookpage{99}.
\item Some of the \textbf{most common machine learning tasks} include \bookpage{100}:
  \begin{itemize}
  \item \textbf{Classification}.
  \item \textbf{Classification} with \textbf{missing inputs}.
  \item \textbf{Regression}.
  \item \textbf{Transcription}.
  \item \textbf{Machine translation}.
  \item \textbf{Structured output}.
  \item \textbf{Anomaly detection}.
  \item \textbf{Imputation} of \textbf{missing values}.
  \item \textbf{Denoising}.
  \end{itemize}
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{The performance measure, P}

\begin{itemize}
\item In order to \textbf{evaluate the abilities} of a \textbf{machine learning algorithm}, we must design a \textbf{quantitative measure} of its \textbf{performance} \bookpage{103}.
\item Usually this \textbf{performance measure P} is \textbf{specific to the task T} being carried out by the system \bookpage{103}.
\item For \textbf{tasks} such as \textbf{classification}, \textbf{classification with missing inputs}, and \textbf{transcription}, we often \textbf{measure} the \textbf{accuracy} of the \textbf{model} \bookpage{103}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{The experience, E}

\begin{itemize}
\item Most of the learning algorithms in this book can be understood as being allowed to experience an entire \textbf{dataset} \bookpage{104}.
\item \textbf{Unsupervised learning} algorithms: experience a dataset containing many features, then \textbf{learn useful properties of the structure} of this dataset \bookpage{105}.
\item \textbf{Supervised learning} algorithms: experience a dataset containing features, but each example is also \textbf{associated} with a \textbf{label} or \textbf{target} \bookpage{105}.
\item \textbf{Reinforcement learning} algorithms: \textbf{interaction with an environment}, so there is a \textbf{feedback loop} between the learning system and its experience \bookpage{106}.
\end{itemize}
\end{frame}

\subsection{Model assessment}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Model assessment}

\epigraph{The \textbf{central challenge} in \textbf{machine learning} is that we must \textbf{perform well on new, previously unseen inputs} — not just those on which our model was trained.}{\bookpage{110}}

\begin{itemize}
\item \textbf{Generalization}: the ability to \textbf{perform well on previously unobserved inputs} \bookpage{110}.
\item \textbf{Underfitting}: when the model is not able to obtain a \textbf{sufficiently low error value} on the \textbf{training set} \bookpage{111}.
\item \textbf{Overfitting}: when the \textbf{gap} between the \textbf{training error} and \textbf{test error} is \textbf{too large} \bookpage{111}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Model improving}

\begin{itemize}
\item \textbf{Regularization}: any \textbf{modification} we make to a learning algorithm that is intended to \textbf{reduce} its \textbf{generalization error} but \textbf{not} its \textbf{training error} \bookpage{120}.
\item \textbf{Cross-validation}: based on the idea of \textbf{repeating the training and testing} computation on \textbf{different randomly chosen subsets} or splits of the original dataset \bookpage{122}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Model properties}

\begin{itemize}
\item \textbf{Bias}: measures the \textbf{expected deviation} from the \textbf{true value} of the function or parameter \bookpage{129}.
\item \textbf{Variance}: a measure of the \textbf{deviation} from the \textbf{expected estimator value} that \textbf{any} particular sampling of the data is \textbf{likely to cause} \bookpage{129}.
\item \textbf{Standard error}: a measure of how we would \textbf{expect} the \textbf{estimate} we compute from data to \textbf{vary} as we \textbf{independently resample the dataset} from the underlying data generating proces \bookpage{128}.
\end{itemize}
\end{frame}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{Model estimators}

\begin{itemize}
\item \textbf{Maximum Likelihood Estimation} (\textbf{MLE}): Rather than \textbf{guessing} that some function \textbf{might make a good estimator} and \textbf{then analyzing its bias and variance}, we would like to have some \textbf{principle from which we can derive specific functions that are good estimators }for different models \bookpage{131}.
\item \textbf{Maximum \textit{A Posteriori}} (\textbf{MAP}): \textbf{Bayesian statistics} uses \textbf{probability} to reflect \textbf{degrees of certainty} of states of knowledge, the true parameter $\theta$ is unknown \bookpage{135}. Rather than simply using the \textbf{MLE}, we can still gain some of the benefit of the \textbf{Bayesian approach} by \textbf{allowing the prior to influence the choice} of the point estimate \bookpage{139}.
\end{itemize}
\end{frame}

\subsection{The end}

% -----------------------------------------------------------------------------
\begin{frame}
\frametitle{The end}

\begin{center}
\large{\textbf{Thank you for your attention...}}
\end{center}

\end{frame}

\end{document}
