\beamer@sectionintoc {1}{Chapter 1: Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Chapters overview - part I.}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Key insights}{3}{0}{1}
\beamer@subsectionintoc {1}{3}{Data representation}{4}{0}{1}
\beamer@subsectionintoc {1}{4}{History}{5}{0}{1}
\beamer@sectionintoc {2}{Chapter 2: Linear Algebra}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{Basic definitions}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Operations with matrices}{8}{0}{2}
\beamer@subsectionintoc {2}{3}{Putting it all together}{11}{0}{2}
\beamer@sectionintoc {3}{Chapter 3: Probability and Information Theory}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Basic probability definitions}{12}{0}{3}
\beamer@subsectionintoc {3}{2}{Basic information theory definitions}{16}{0}{3}
\beamer@subsectionintoc {3}{3}{Probabilistic models}{17}{0}{3}
\beamer@sectionintoc {4}{Chapter 4: Numerical Computation}{19}{0}{4}
\beamer@subsectionintoc {4}{1}{Basic terminology}{19}{0}{4}
\beamer@subsectionintoc {4}{2}{Optimization}{20}{0}{4}
\beamer@subsectionintoc {4}{3}{Useful formulas}{24}{0}{4}
\beamer@sectionintoc {5}{Chapter 5: Machine Learning Basics}{25}{0}{5}
\beamer@subsectionintoc {5}{1}{Basic introduction}{25}{0}{5}
\beamer@subsectionintoc {5}{2}{Model assessment}{29}{0}{5}
\beamer@subsectionintoc {5}{3}{The end}{33}{0}{5}
